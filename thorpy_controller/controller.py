import astropy.units as u
import enum
from thorpy.comm.discovery import discover_stages


class Borg:
    _shared_state = {}

    def __init__(self):
        self.__dict__ = self._shared_state


class TriggerMode(enum.Enum):
    disabled = 0
    GPIO_input = 1
    relative_move = 2
    absolute_move = 3
    home_move = 4
    GPIO_output = 0x0A
    in_motion = 0x0B
    max_velocity = 0x0C
    pulsed_forwards = 0x0D
    pulsed_backwards = 0x0E
    pulsed_both = 0x0F


class TriggerPolarity(enum.Enum):
    active_low = 0
    active_high = 1


class Controller(Borg):
    def __init__(self):
        super().__init__()

        if not hasattr(self, "_stage"):
            stages = list(discover_stages())
            if len(stages) != 1:
                raise Exception(f"Did not find a single stage, instead {stages}")

            self._stage = stages[0]

            # perform a homing run to detect the 0 position correctly
            self._stage.home()

            # units used for conversion to internal factors used for the device
            # these are adjusted form the DDR25, usually they should be
            # without the 360
            self._position_unit = 360 * u.deg
            self._velocity_unit = 360 * u.deg / u.s
            self._acceleration_unit = u.deg / u.s ** 2

    @property
    def homed(self):
        return self._stage.status_homed

    @property
    def position(self):
        return (self._stage.position * self._position_unit).to(u.deg)

    @position.setter
    def position(self, value):
        self._stage.position = value.to_value(self._position_unit)

    @property
    def velocity(self):
        return (self._stage.max_velocity * self._velocity_unit).to(u.deg / u.s)

    @velocity.setter
    def velocity(self, value):
        self._stage.max_velocity = value.to_value(self._velocity_unit)

    @property
    def acceleration(self):
        return (self._stage.acceleration * self._acceleration_unit).to(u.deg / u.s ** 2)

    @acceleration.setter
    def acceleration(self, value):
        self._stage.acceleration = value.to_value(self._acceleration_unit)

    @property
    def move_relative_distance(self):
        return (self._stage.move_relative_distance * self._position_unit).to(u.deg)

    @move_relative_distance.setter
    def move_relative_distance(self, value):
        self._stage.move_relative_distance = value.to_value(self._position_unit)

    @property
    def move_absolute_position(self):
        return (self._stage.move_absolute_position * self._position_unit).to(u.deg)

    @move_absolute_position.setter
    def move_absolute_position(self, value):
        self._stage.move_absolute_position = value.to_value(self._position_unit)

    @property
    def trigger1_mode(self):
        return TriggerMode(self._stage.trig_1_mode)

    @trigger1_mode.setter
    def trigger1_mode(self, value: TriggerMode):
        self._stage.trig_1_mode = value.value

    @property
    def trigger1_polarity(self):
        return TriggerPolarity(self._stage.trig_1_polarity)

    @trigger1_polarity.setter
    def trigger1_polarity(self, value):
        self._stage.trig_1_polarity = value.value

    @property
    def trigger2_mode(self):
        return TriggerMode(self._stage.trig_2_mode)

    @trigger2_mode.setter
    def trigger2_mode(self, value: TriggerMode):
        self._stage.trig_2_mode = value.value

    @property
    def trigger2_polarity(self):
        return TriggerPolarity(self._stage.trig_2_polarity)

    @trigger2_polarity.setter
    def trigger2_polarity(self, value):
        self._stage.trig_2_polarity = value.value
