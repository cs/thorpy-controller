import astropy.units as u
from flask import Flask
from flask_restful import Api, fields, marshal, marshal_with, Resource, reqparse


from .controller import Controller, TriggerPolarity, TriggerMode


app = Flask(__name__)
api = Api(app)

# hold a global handle to the controller to make sure the connection
# is staying alive
c = Controller()

unit_fields = dict(value=fields.Float, unit=fields.String)

device_fields = dict(
    homed=fields.Boolean,
#    enabled=fields.Boolean,
    position=fields.Nested(unit_fields),
    velocity=fields.Nested(unit_fields),
    acceleration=fields.Nested(unit_fields),
    move_relative_distance=fields.Nested(unit_fields),
    move_absolute_position=fields.Nested(unit_fields),
)

unit_parser = reqparse.RequestParser()
unit_parser.add_argument("value", type=float, required=True)
unit_parser.add_argument("unit", type=str, store_missing=False)


class ThorpyDevice(Resource):
    def get(self):
        c = Controller()
        out = marshal(c, device_fields, envelope="resource")
        n1 = c.trigger1_mode.name
        n2 = c.trigger2_mode.name
        try:
            p1 = c.trigger1_polarity.name
            p2 = c.trigger2_polarity.name
        except:
            p1 = 100
            p2 = 100
        out["resource"].update(
            {
                1: {"mode": n1, "polarity": p1},
#                1: {"mode": c.trigger1_mode.name, "polarity": c.trigger1_polarity.name},
                2: {"mode": n2, "polarity": p2},
#                2: {"mode": c.trigger2_mode.name, "polarity": c.trigger2_polarity.name},
            }
        )
        return out


class ThorpyUnitValue(Resource):
    @marshal_with(unit_fields, envelope="resource")
    def get(self):
        return getattr(Controller(), self._name)

    def put(self):
        args = unit_parser.parse_args(strict=True)
        c = Controller()
        setattr(c, self._name, args["value"] * u.Unit(args.get("unit", self._unit)))
        return {"message": {"result": "ok"}}


class ThorpyPosition(ThorpyUnitValue):
    _name = "position"
    _unit = u.deg


class ThorpyVelocity(ThorpyUnitValue):
    _name = "velocity"
    _unit = u.deg / u.s


class ThorpyAcceleration(ThorpyUnitValue):
    _name = "acceleration"
    _unit = u.deg / u.s ** 2


class ThorpyMoveRelative(ThorpyUnitValue):
    _name = "move_relative_distance"
    _unit = u.deg


class ThorpyMoveAbsolute(ThorpyUnitValue):
    _name = "move_absolute_position"
    _unit = u.deg


class ThorpyEnumValue(Resource):
    def get(self, number):
        return {
            "resource": {
                self._display_name: getattr(Controller(), self._name.format(number=number)).name
            }
        }

    def put(self, number):
        parser = reqparse.RequestParser()
        parser.add_argument(
            "value", type=str, required=True, choices=self._enum.__members__.keys()
        )
        args = parser.parse_args(strict=True)
        setattr(Controller(), self._name.format(number=number), self._enum[args["value"]])
        return {"message": {"result": "ok"}}


class ThorpyTriggerMode(ThorpyEnumValue):
    _display_name = "mode"
    _name = "trigger{number}_mode"
    _enum = TriggerMode


class ThorpyTriggerPolarity(ThorpyEnumValue):
    _display_name = "polarity"
    _name = "trigger{number}_polarity"
    _enum = TriggerPolarity


class ThorpyTrigger(Resource):
    def get(self, number):
        c = Controller()
        return {
            "resource": {
                "mode": getattr(c, f"trigger{number}_mode").name,
                "polarity": getattr(c, f"trigger{number}_polarity").name,
            }
        }


api.add_resource(ThorpyDevice, "/device")
api.add_resource(ThorpyPosition, "/position")
api.add_resource(ThorpyVelocity, "/velocity")
api.add_resource(ThorpyAcceleration, "/acceleration")
api.add_resource(ThorpyMoveRelative, "/move/relative")
api.add_resource(ThorpyMoveAbsolute, "/move/absolute")
api.add_resource(ThorpyTrigger, "/trigger/<int:number>")
api.add_resource(ThorpyTriggerMode, "/trigger/<int:number>/mode")
api.add_resource(ThorpyTriggerPolarity, "/trigger/<int:number>/polarity")


if __name__ == "__main__":
    app.run(debug=True)
