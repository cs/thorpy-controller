# Thorpy controller
This is a simple flask based server to expose a device connected via the
Thorlabs APT protocol as a REST service. It is currently only tested with
the DDR25, connected to a KBD101. For this to work it requires our [patched
thorpy](https://gitlab.physik.uni-muenchen.de/cs/thorpy) version.