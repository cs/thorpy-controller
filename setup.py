from setuptools import setup, find_packages

with open("requirements.txt", "r") as f:
    requirements = f.readlines()

setup(
    name="thorpy_controller",
    version="1.0",
    description="controller for thorpy devices providing simple REST interface",
    author="Hendrik v. Raven",
    author_email="hendrik@consetetur.de",
    packages=find_packages(),
    install_requires=requirements,
)
